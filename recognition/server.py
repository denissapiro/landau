import pika, random

credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters('rabbit',
                                       5672,
                                       '/',
                                       credentials)
try:
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='for_recognition')
    channel.queue_declare(queue='output')


    def callback(ch, method, properties, body):
        import datetime
        channel.basic_publish(exchange='',
                              routing_key='output',
                              body=str(body) +
                                   ' salt from recognition ('
                                   + str(datetime.datetime.now()) + '): '
                                   + str(random.random()))


    channel.basic_consume(queue='for_recognition',
                          auto_ack=True,
                          on_message_callback=callback)

    channel.start_consuming()
except pika.exceptions.AMQPConnectionError:
    print("recognition: Waiting for rabbit server")

#!/usr/bin/env python3
import pika, time, datetime
credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters('rabbit',
                                       5672,
                                       '/',
                                       credentials)

try:
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='for_recognition')

    while True:
        time.sleep(2)

        channel.basic_publish(exchange='',
                              routing_key='for_recognition',
                              body='Please recognize the /1.mp3!')

        print(str(datetime.datetime.now()) + ": [x] Sent message")

except pika.exceptions.AMQPConnectionError:
    print("listener: Waiting for rabbit server")
import pika

credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters('rabbit', 5672, '/', credentials)
try:
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='output')


    def callback(ch, method, properties, body):
        import datetime
        print(str(datetime.datetime.now()) + ": [x] Received %r" % body)


    channel.basic_consume(queue='output', auto_ack=True, on_message_callback=callback)

    channel.start_consuming()
except pika.exceptions.AMQPConnectionError:
    print("output: Waiting for rabbit server")


